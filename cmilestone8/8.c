#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 

int int_cmp(const void *a, const void *b);
void sortint();
int cstring_cmp(const void *a, const void *b);
void sortstr();

 int main() 
{ 
    sortint();
    sortstr();
    return 0;
} 
 
 
int int_cmp(const void *a, const void *b) 
{ 
    const int *ia = (const int *)a; 
    const int *ib = (const int *)b;
    return *ia  - *ib; 
	 
} 
 


 
 
void sortint() 
{ 
    int nums[] = { 9, 3, 5, 1, -1, 45, 42, 43, 2, -7, 6 }; 
    int len = sizeof(nums)/sizeof(int);
    printf("sorting");
    printf("\n");
    int i;
    for(i=0; i<len; i++) 
    {
        printf("%d,", nums[i]);
    }
    printf("\n");
    qsort(nums, len, sizeof(int), int_cmp);
    for(i=0; i<len; i++) 
    {
        printf("%d,", nums[i]);
    } 
    printf("\n");
} 
 

int cstring_cmp(const void *a, const void *b) 
{ 
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
	
} 
 

 

void sortstr() 
{ 
    char *strings[] = { "Shreyas", "Anup", "Kapil", "Ashutosh", "Atharva", "Ninad" };
    int len = sizeof(strings) / sizeof(char*);
    printf("sorting STRING");
	printf("\n");
    int i;
    for(i=0; i<len; i++) 
        printf("%s,", strings[i]);
    printf("\n");
    qsort(strings, len, sizeof(char *), cstring_cmp);
    for(i=0; i<len; i++) 
        printf("%s,", strings[i]);
    printf("\n");
} 

 